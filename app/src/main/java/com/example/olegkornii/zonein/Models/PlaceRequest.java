package com.example.olegkornii.zonein.Models;

/**
 * Created by Oleg Kornii on 11.08.2016.
 */
public class PlaceRequest {
        private String name;
        private String description;
        private double latitude;
        private double longitude;

        public PlaceRequest(String name, String description, double latitude, double longitude) {
            this.name = name;
            this.description = description;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public String getName() {
            return name;
        }

        public String getDesciption() {
            return description;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

    }

