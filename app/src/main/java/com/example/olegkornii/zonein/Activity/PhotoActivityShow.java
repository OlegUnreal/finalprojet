package com.example.olegkornii.zonein.Activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.SortedList;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.olegkornii.zonein.Adapters.AdapterShowImage;
import com.example.olegkornii.zonein.Models.Constants;
import com.example.olegkornii.zonein.Models.ImageModel;
import com.example.olegkornii.zonein.Models.ImageModelForShow;
import com.example.olegkornii.zonein.Models.LocationService;
import com.example.olegkornii.zonein.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Oleg Kornii on 18.08.2016.
 */
public class PhotoActivityShow extends AppCompatActivity {
    private List<ImageModel.ItemImage> imageList = new ArrayList<>();
    private List<ImageModelForShow> imageModelForShows = new ArrayList<>();
    private String
            getIdPlace;
    private ImageView showPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_show_image);
        initUI();
        final AdapterShowImage adapter = new AdapterShowImage(imageModelForShows, getApplicationContext());
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewShow);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LocationService service = retrofit.create(LocationService.class);
        final Call<ImageModel> image = service.getImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
        image.enqueue(new Callback<ImageModel>() {
                          @Override
                          public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                              ImageModel imageModel = response.body();
                              imageList = imageModel.getData();
                              int i;
                              for (i = 0; i < imageList.size(); i++) {
                                  String imm = imageList.get(i).getConnectedPlace();
                                  if (imageList.get(i).getConnectedPlace().equals(getIdPlace)) {

                                      try {
                                          imageModelForShows.add(new ImageModelForShow(Constants.BASE_URL + Constants.API_APPLICATION_ID + "/v1/files/" + imageList.get(i).getFullPath() + "/" + imageList.get(i).getOwnerId()));
                                          adapter.notifyDataSetChanged();
                                      } catch (Exception e) {
                                          Toast.makeText(getApplicationContext(), "False data image", Toast.LENGTH_LONG).show();
                                      }
                                  }
                              }
                          }

                          @Override
                          public void onFailure(Call<ImageModel> call, Throwable t) {
                              Toast.makeText(getApplicationContext(), "No data of image, pleas refreash application", Toast.LENGTH_LONG).show();
                          }
                      }

        );
    }

    private void initUI() {
        getIdPlace = getIntent().getStringExtra("idPl");
        showPhotos = (ImageView) findViewById(R.id.showPhotos);

    }
}
