package com.example.olegkornii.zonein.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.view.menu.MenuView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessException;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.bumptech.glide.Glide;
import com.example.olegkornii.zonein.Models.CityLocation;
import com.example.olegkornii.zonein.Models.Constants;
import com.example.olegkornii.zonein.Models.ImageModel;
import com.example.olegkornii.zonein.Models.ImageRequest;
import com.example.olegkornii.zonein.Models.LocationService;
import com.example.olegkornii.zonein.Models.PeopleModel;
import com.example.olegkornii.zonein.Models.PlacesModel;
import com.example.olegkornii.zonein.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {
    private GoogleMap mMap;
    private List<PlacesModel.ItemPlace> placeModel = new ArrayList<>();
    private List<PeopleModel.ItemPeople> peopleModels = new ArrayList<>();
    private List<ImageModel.ItemImage> imageList = new ArrayList<>();
    private ImageView
            av;
    private TextView
            nameOfPeople,
            emailOfPeople;
    private final int
            RESULT_PIC_FROM_CAMERA = 1,
            RESULT_PIC_ROM_GALARY = 2;
    private ImageView
            imageView;
    private String
            getLon,
            getLat,
            getCityName,
            email,
            title,
            time,
            path,
            pNameCam,
            objectId;
    private Double
            getDobLon,
            getDobLat;

    private MenuView.ItemView
            iMap,
            iPeople,
            iPlaces,
            iLogOut;
    private Button
            searchCityBut;
    private EditText
            searcCityEdit;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Maps");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        initUI();
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hed = navigationView.getHeaderView(0);
        final TextView na = (TextView) hed.findViewById(R.id.NameOfUser);
        TextView em = (TextView) hed.findViewById(R.id.EmailOfUser);
        av = (ImageView) hed.findViewById(R.id.imageView);
        av.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppTheme_AppBarOverlay)
                        .setTitle("Do you want to choose avatar?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this, R.style.AppTheme_AppBarOverlay)
                                        .setTitle("Choose image")
                                        .setPositiveButton("Choose image from camera", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intentCam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                Date date = new Date(System.currentTimeMillis());
                                                time = String.valueOf(date.getTime());
                                                File file = new File(Environment.getExternalStorageDirectory(), time + ".png");
                                                Uri photodir = Uri.fromFile(file);
                                                path = photodir.toString();
                                                Uri photoPath = Uri.parse(path);
                                                intentCam.putExtra(MediaStore.EXTRA_OUTPUT, path);
                                                startActivityForResult(intentCam, RESULT_PIC_FROM_CAMERA);


                                            }
                                        })
                                        .setNegativeButton("Choose image from gallary", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intentGal = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                                startActivityForResult(intentGal, RESULT_PIC_ROM_GALARY);
                                            }
                                        });
                                builder2.show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                builder.show();


                return false;
            }
        });

        em.setText(email);

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LocationService service = retrofit.create(LocationService.class);
        final Call<PeopleModel> user = service.getUserByAppId(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
        user.enqueue(new Callback<PeopleModel>() {
                         @Override
                         public void onResponse(Call<PeopleModel> call, Response<PeopleModel> response) {
                             PeopleModel people = response.body();
                             peopleModels = people.getData();
                             for (int i = 0; i < peopleModels.size(); i++) {
                                 if (peopleModels.get(i).getEmail().equals(email)) {
                                     try {
                                         na.setText(peopleModels.get(i).getName().toString());

                                     } catch (Exception e) {
                                         Toast.makeText(getApplicationContext(), "Warning:loaded wrong name, please refreash application!", Toast.LENGTH_SHORT).show();
                                     }

                                 }
                             }
                             for (int i = 0; i < peopleModels.size(); i++) {
                                 if (peopleModels.get(i).getEmail().equals(email)) {
                                     objectId = peopleModels.get(i).getObjectId().toString();
                                 }
                                 break;
                             }
                             final Retrofit retrofit = new Retrofit.Builder()
                                     .baseUrl(Constants.BASE_URL)
                                     .addConverterFactory(GsonConverterFactory.create())
                                     .build();
                             LocationService service = retrofit.create(LocationService.class);
                             final Call<ImageModel> image = service.getImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                             image.enqueue(new Callback<ImageModel>() {
                                               @Override
                                               public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                                                   ImageModel imageModel = response.body();
                                                   imageList = imageModel.getData();
                                                   int i;
//

                                                   for (i = 0; i < imageList.size(); i++) {
                                                       if (imageList.get(i).getConnectedPlace().equals(objectId)) {
//
                                                           try {
                                                               Glide
                                                                       .with(getApplicationContext())
                                                                       .load(Constants.BASE_URL + Constants.API_APPLICATION_ID + "/v1/files/" + imageList.get(i).getFullPath() + "/" + imageList.get(i).getOwnerId())

                                                                       .into(av);
                                                           } catch (Exception e) {

                                                               Toast.makeText(getApplicationContext(), "False data image", Toast.LENGTH_LONG).show();
                                                           }
                                                       }
                                                       break;
                                                   }
//
                                               }

                                               @Override
                                               public void onFailure(Call<ImageModel> call, Throwable t) {
                                                   Toast.makeText(getApplicationContext(), "No data of image, pleas refreash application", Toast.LENGTH_LONG).show();
                                               }
                                           }

                             );

//                                 }
//                             }

                         }

                         @Override
                         public void onFailure(Call<PeopleModel> call, Throwable t) {
                             Toast.makeText(getApplicationContext(), "Warning:Name don't load,please refreash application!", Toast.LENGTH_SHORT).show();
                         }
                     }

        );


        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if (getLat != null && getLon != null && getCityName != null) {
            getDobLat = Double.valueOf(getLat);
            getDobLon = Double.valueOf(getLon);
        }


    }

    private void initUI() {
        iMap = (MenuView.ItemView) findViewById(R.id.nav_map);
        iPeople = (MenuView.ItemView) findViewById(R.id.nav_people);
        iPlaces = (MenuView.ItemView) findViewById(R.id.nav_places);
        iLogOut = (MenuView.ItemView) findViewById(R.id.logout);
        searcCityEdit = (EditText) findViewById(R.id.searcCityEditt);
        searchCityBut = (Button) findViewById(R.id.searchCityButt);
        nameOfPeople = (TextView) findViewById(R.id.NameOfUser);
        emailOfPeople = (TextView) findViewById(R.id.EmailOfUser);
        imageView = (ImageView) findViewById(R.id.imageView);
        searchCityBut = (Button) findViewById(R.id.searchCityButt);
        searcCityEdit = (EditText) findViewById(R.id.searcCityEditt);
        getLat = getIntent().getStringExtra("Lat");
        getLon = getIntent().getStringExtra("Lon");
        getCityName = getIntent().getStringExtra("CityName");
        email = getIntent().getStringExtra("email");
        title = getIntent().getStringExtra("title");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RESULT_PIC_FROM_CAMERA:
                if (resultCode == RESULT_OK) {
                    final Bitmap photo = (Bitmap) data.getExtras().get("data");
                    Toast.makeText(getApplicationContext(), photo.toString(), Toast.LENGTH_SHORT).show();
                    Backendless.Files.Android.upload(photo,
                            Bitmap.CompressFormat.PNG,
                            100,
                            pNameCam = photo.hashCode() + ".png",
                            path,
//
                            new AsyncCallback<BackendlessFile>() {
                                @Override
                                public void handleResponse(final BackendlessFile backendlessFile) {
                                    Glide
                                            .with(MainActivity.this)
                                            .load("https://api.backendless.com/892B27F2-C4C2-42E0-FF52-5EAD8C9A4400/v1/files/" + path.toString() + "/" + pNameCam)

                                            .error(R.mipmap.ic_launcher)
                                            .skipMemoryCache(false)
                                            .into(av);
                                    Toast.makeText(getApplicationContext(), "Photo loaded success!", Toast.LENGTH_SHORT).show();

//
                                }

                                //
                                @Override
                                public void handleFault(BackendlessFault backendlessFault) {
                                    Toast.makeText(MainActivity.this, backendlessFault.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
//
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    LocationService service = retrofit.create(LocationService.class);
                    ImageRequest request = new ImageRequest(
                            objectId, path, pNameCam);
                    Log.e("TAG", new Gson().toJson(request));
                    Call<ImageModel> userInfo = service.createImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY, request);
                    userInfo.enqueue(new Callback<ImageModel>() {

                        @Override
                        public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                            Log.e("TAG", String.valueOf(response.raw()));
                            if (response.isSuccessful()) {
                            }
                        }

                        @Override
                        public void onFailure(Call<ImageModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                break;
            case RESULT_PIC_ROM_GALARY:
                final String photoPath = getFirstImage(data);
                Bitmap photo = BitmapFactory.decodeFile(photoPath);
                Toast.makeText(getApplicationContext(), photoPath, Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), photo.toString(), Toast.LENGTH_SHORT).show();
                Backendless.Files.Android.upload(photo,
                        Bitmap.CompressFormat.PNG,
                        100,
                        pNameCam = photo.hashCode() + ".png",
                        photoPath,

                        new AsyncCallback<BackendlessFile>() {
                            @Override
                            public void handleResponse(final BackendlessFile backendlessFile) {
                                Glide
                                        .with(MainActivity.this)
                                        .load("https://api.backendless.com/892B27F2-C4C2-42E0-FF52-5EAD8C9A4400/v1/files/" + photoPath + "/" + pNameCam)

                                        .error(R.mipmap.ic_launcher)
                                        .skipMemoryCache(false)
                                        .into(av);
                                Toast.makeText(getApplicationContext(), "Photo loaded success!", Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void handleFault(BackendlessFault backendlessFault) {
                                Toast.makeText(MainActivity.this, backendlessFault.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                LocationService service = retrofit.create(LocationService.class);
                ImageRequest request = new ImageRequest(
                        objectId, photoPath, pNameCam);
                Log.e("TAG", new Gson().toJson(request));
                Call<ImageModel> userInfo = service.createImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY, request);
                userInfo.enqueue(new Callback<ImageModel>() {

                    @Override
                    public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                        Log.e("TAG", String.valueOf(response.raw()));
                        if (response.isSuccessful()) {
                        }
                    }

                    @Override
                    public void onFailure(Call<ImageModel> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                    }
                });

                break;
        }

    }

    private String getFirstImage(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);

        if (cursor == null || cursor.getCount() < 1) {
            return null;
        }

        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        if (columnIndex < 0)
            return null;

        String picturePath = cursor.getString(columnIndex);

        cursor.close(); // close cursor
        return picturePath;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_people) {
            Intent intent = new Intent(MainActivity.this, PeopleActivity.class);
            startActivity(intent);
        }
        if (id == R.id.logout) {
            BackendlessUser user;
            try {
                Backendless.UserService.logout();
            } catch (BackendlessException exception) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        }
        if (id == R.id.nav_map) {
            Intent intent = new Intent(MainActivity.this, MapsActivity.class);
            startActivity(intent);
        }
        if (id == R.id.nav_places) {
            Intent intent = new Intent(MainActivity.this, PlacesActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (getLat == null && getLon == null && getCityName == null) {

            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            LocationService service = retrofit.create(LocationService.class);
            final Call<PlacesModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
            user.enqueue(new Callback<PlacesModel>() {
                @Override
                public void onResponse(Call<PlacesModel> call, Response<PlacesModel> response) {
                    PlacesModel people = response.body();
                    placeModel = people.getData();
                    for (int i = 0; i < placeModel.size(); i++) {
                        LatLng city = new LatLng(Double.valueOf(placeModel.get(i).getLatitude()), Double.valueOf(placeModel.get(i).getLongitude()));
                        mMap.addMarker(new MarkerOptions()
                                .position(city)
                                .title(placeModel.get(i).getName())
                        );
                    }
                }

                @Override
                public void onFailure(Call<PlacesModel> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                }
            });
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {

                    final Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    LocationService service = retrofit.create(LocationService.class);
                    final Call<PlacesModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                    user.enqueue(new Callback<PlacesModel>() {
                        @Override
                        public void onResponse(Call<PlacesModel> call, Response<PlacesModel> response) {
                            PlacesModel people = response.body();
                            placeModel = people.getData();
                            for (int i = 0; i < placeModel.size(); i++) {
                                if (placeModel.get(i).getName().equals(marker.getTitle())) {
                                    Intent intent = new Intent(MainActivity.this, PhotoActivityShow.class);
                                    intent.putExtra("idPl", placeModel.get(i).getObjectId());
                                    startActivity(intent);
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<PlacesModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });


                    return false;
                }
            });

            searchCityBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    LocationService service = retrofit.create(LocationService.class);
                    Call<CityLocation> weather = service.getWeatherByName(searcCityEdit.getText().toString(), Constants.API_APPLICATION_ID_LATLON);
                    weather.enqueue(new Callback<CityLocation>() {
                        @Override
                        public void onResponse(Call<CityLocation> call, Response<CityLocation> response) {
                            CityLocation cityCoords = response.body();
                            LatLng latLng = new LatLng(cityCoords.getCoord().getLat(), cityCoords.getCoord().getLon());
                            mMap.addMarker(new MarkerOptions().position(latLng).title(searcCityEdit.getText().toString()));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        }

                        @Override
                        public void onFailure(Call<CityLocation> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT);
                        }
                    });
                }
            });
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    LatLng city = new LatLng(latLng.latitude, latLng.longitude);
                    mMap.addMarker(new MarkerOptions().position(city).title(title));
                    Intent intent = new Intent(MainActivity.this, NewPlaceActivity.class);
                    intent.putExtra("lat", latLng.latitude);
                    intent.putExtra("long", latLng.longitude);
                    startActivity(intent);
                }
            });

        } else {


            LatLng city = new LatLng(getDobLat, getDobLon);

            mMap.addMarker(new MarkerOptions()
                    .position(city)
                    .title(getCityName)
            );
            mMap.moveCamera(CameraUpdateFactory.newLatLng(city));
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    LatLng city = new LatLng(latLng.latitude, latLng.longitude);
                    mMap.addMarker(new MarkerOptions().position(city).title(title));
                    Intent intent = new Intent(MainActivity.this, NewPlaceActivity.class);
                    intent.putExtra("lat", latLng.latitude);
                    intent.putExtra("long", latLng.longitude);
                    startActivity(intent);
                }
            });

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {

                    final Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    LocationService service = retrofit.create(LocationService.class);
                    final Call<PlacesModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                    user.enqueue(new Callback<PlacesModel>() {
                        @Override
                        public void onResponse(Call<PlacesModel> call, Response<PlacesModel> response) {
                            PlacesModel people = response.body();
                            placeModel = people.getData();
                            for (int i = 0; i < placeModel.size(); i++) {
                                if (placeModel.get(i).getName().equals(marker.getTitle())) {
                                    Intent intent = new Intent(MainActivity.this, PhotoActivityShow.class);
                                    intent.putExtra("idPl", placeModel.get(i).getObjectId());
                                    startActivity(intent);
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<PlacesModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });


                    return false;
                }
            });
        }
    }

}
