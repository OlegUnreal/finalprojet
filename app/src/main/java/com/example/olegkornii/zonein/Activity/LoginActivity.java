package com.example.olegkornii.zonein.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.example.olegkornii.zonein.Models.Constants;
import com.example.olegkornii.zonein.R;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText
            email,
            password;
    private Button
            signIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login In");
        Backendless.initApp(LoginActivity.this, Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY, "v1");
        initUI();
        setOnClickListener();
        setFonts();

    }

    private void initUI() {
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        signIn = (Button) findViewById(R.id.email_sign_in_button);
        email.setText("olehspidey@gmail.com");
        password.setText("1234");
    }

    private void setOnClickListener() {
        signIn.setOnClickListener(this);
    }

    private void setFonts() {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.email_sign_in_button:
                String em = email.getText().toString();
                String pas = password.getText().toString();
                if (!em.equals("") && !pas.equals("")) {
                    Backendless.UserService.login(em, pas, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser response) {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra("email", email.getText().toString());
                            startActivity(intent);
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Toast.makeText(getApplicationContext(), fault.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
        }
    }
}
