package com.example.olegkornii.zonein.Models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.olegkornii.zonein.Adapters.AdapterFriends;
import com.example.olegkornii.zonein.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Oleg Kornii on 28.08.2016.
 */
public class FriendsModel implements Parcelable {

    private List<ItemPeople> data;

    protected FriendsModel(Parcel in) {
        data = in.createTypedArrayList(ItemPeople.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<PeopleModel> CREATOR = new Creator<PeopleModel>() {
        @Override
        public PeopleModel createFromParcel(Parcel in) {
            return new PeopleModel(in);
        }

        @Override
        public PeopleModel[] newArray(int size) {
            return new PeopleModel[size];
        }
    };

    public List<ItemPeople> getData() {
        return data;
    }

    public static class ItemPeople implements Parcelable {


        private String
                name;
        private String email;

        public String getObjectId() {
            return objectId;
        }

        private String objectId;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public ItemPeople(String name, String email, String objectId) {
            this.name = name;
            this.email = email;
            this.objectId = objectId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        protected ItemPeople(Parcel in) {
            name = in.readString();
            email = in.readString();
            objectId = in.readString();
        }

        public static final Creator<ItemPeople> CREATOR = new Creator<ItemPeople>() {
            @Override
            public ItemPeople createFromParcel(Parcel in) {
                return new ItemPeople(in);
            }

            @Override
            public ItemPeople[] newArray(int size) {
                return new ItemPeople[size];
            }
        };


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(email);
            dest.writeString(objectId);
        }
    }
}
