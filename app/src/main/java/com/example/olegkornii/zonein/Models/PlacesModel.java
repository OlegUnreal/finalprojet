package com.example.olegkornii.zonein.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Oleg Kornii on 14.08.2016.
 */
public class PlacesModel implements Parcelable {
    private List<ItemPlace> data;

    protected PlacesModel(Parcel in) {
        data = in.createTypedArrayList(ItemPlace.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlacesModel> CREATOR = new Creator<PlacesModel>() {
        @Override
        public PlacesModel createFromParcel(Parcel in) {
            return new PlacesModel(in);
        }

        @Override
        public PlacesModel[] newArray(int size) {
            return new PlacesModel[size];
        }
    };

    public List<ItemPlace> getData() {
        return data;
    }

    public static class ItemPlace implements Parcelable {
        private String name,
                description,
                latitude,
                longitude,
                objectId;

        public String getObjectId() {
            return objectId;
        }

        public void setObjectId(String objectId) {
            this.objectId = objectId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        protected ItemPlace(Parcel in) {
            name = in.readString();
            description = in.readString();
            latitude = in.readString();
            longitude = in.readString();
            objectId = in.readString();
        }

        public static final Creator<ItemPlace> CREATOR = new Creator<ItemPlace>() {
            @Override
            public ItemPlace createFromParcel(Parcel in) {
                return new ItemPlace(in);
            }

            @Override
            public ItemPlace[] newArray(int size) {
                return new ItemPlace[size];
            }
        };


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(description);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(objectId);

        }
    }
}
