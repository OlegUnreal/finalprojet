package com.example.olegkornii.zonein.Models;

/**
 * Created by Oleg Kornii on 18.08.2016.
 */
public class ImageRequest {
    private String
            connectedPlace,
            fullPath,
            ownerId;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String namePic) {
        this.ownerId = ownerId;
    }

    public String setConnectedPlace() {
        return connectedPlace;
    }

    public void getConnectedPlace(String imageModel) {
        this.connectedPlace = imageModel;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public ImageRequest(String connectedPlace, String fullPath, String ownerId) {
        this.connectedPlace = connectedPlace;
        this.fullPath = fullPath;
        this.ownerId = ownerId;
    }
}