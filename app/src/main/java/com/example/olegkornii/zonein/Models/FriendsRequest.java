package com.example.olegkornii.zonein.Models;

/**
 * Created by Oleg Kornii on 28.08.2016.
 */
public class FriendsRequest {
    String
            name,
            email,
            objectId;

    public FriendsRequest(String name, String email, String objectId) {
        this.name = name;
        this.email = email;
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}

