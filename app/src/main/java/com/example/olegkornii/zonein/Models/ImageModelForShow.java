package com.example.olegkornii.zonein.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Oleg Kornii on 18.08.2016.
 */
public class ImageModelForShow implements Parcelable {

    String linkPhoto;

    public ImageModelForShow(String linkPhoto) {
        this.linkPhoto = linkPhoto;
    }

    public String getLinkPhoto() {
        return linkPhoto;
    }

    public void setLinkPhoto(String linkPhoto) {
        this.linkPhoto = linkPhoto;
    }

    protected ImageModelForShow(Parcel in) {
        linkPhoto = in.readString();

    }

    public static final Creator<ImageModelForShow> CREATOR = new Creator<ImageModelForShow>() {
        @Override
        public ImageModelForShow createFromParcel(Parcel in) {
            return new ImageModelForShow(in);
        }

        @Override
        public ImageModelForShow[] newArray(int size) {
            return new ImageModelForShow[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(linkPhoto);
    }
}
