package com.example.olegkornii.zonein.Models;

import com.example.olegkornii.zonein.Activity.NewPlaceActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Oleg Kornii on 03.08.2016.
 */
public interface LocationService {
    @GET("data/2.5/weather")
    Call<CityLocation> getWeatherByName(@Query("q") String city, @Query("appid") String token);

    //    @GET("v1/data/Users")
//    Call<RequestValue> getUserByAppId(@Query("secret-key") String secretKey, @Query("application-id") String applicationId);
    @GET("v1/data/Users")
    Call<PeopleModel> getUserByAppId(@Header("application-id") String appId, @Header("secret-key") String secKey);

    @GET("v1/data/Users")
    Call<FriendsModel> getFriendsByAppId(@Header("application-id") String appId, @Header("secret-key") String secKey);

    @GET("v1/data/Places")
    Call<PlacesModel> getPlaceByAddIp(@Header("application-id") String appId, @Header("secret-key") String secKey);

    @GET("v1/data/Image")
    Call<ImageModel> getImage(@Header("application-id") String appId, @Header("secret-key") String secKey);

    @Headers({"Content-Type: application/json"})
    @POST("v1/data/Places")
    Call<PlacesModel> createPlace(@Header("application-id") String appid, @Header("secret-key") String sKey, @Body PlaceRequest pl);

    @Headers({"Content-Type: application/json"})
    @POST("v1/data/Image")
    Call<ImageModel> createImage(@Header("application-id") String appid, @Header("secret-key") String sKey, @Body ImageRequest ir);

    @Headers({"Content-Type: application/json"})
    @POST("v1/data/Places")
    Call<FriendsModel> createFriends(@Header("application-id") String appid, @Header("secret-key") String sKey, @Body FriendsRequest fr);

}
