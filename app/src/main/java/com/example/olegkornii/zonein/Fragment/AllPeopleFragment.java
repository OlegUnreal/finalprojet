package com.example.olegkornii.zonein.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.olegkornii.zonein.Adapters.AdapterPeople;
import com.example.olegkornii.zonein.Models.Constants;
import com.example.olegkornii.zonein.Models.LocationService;
import com.example.olegkornii.zonein.Models.PeopleModel;
import com.example.olegkornii.zonein.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AllPeopleFragment extends Fragment {
    private List<PeopleModel.ItemPeople> peopleModels = new ArrayList<>();
    public AllPeopleFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_people, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewPeople);
        final AdapterPeople adapter = new AdapterPeople(peopleModels);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.backendless.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LocationService service = retrofit.create(LocationService.class);
        final Call<PeopleModel> user = service.getUserByAppId(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
//        objectId = "0A1E2B86-8410-512E-FF11-F00D98B29F00";
        user.enqueue(new Callback<PeopleModel>() {
            @Override
            public void onResponse(Call<PeopleModel> call, Response<PeopleModel> response) {
                PeopleModel people = response.body();
                peopleModels = people.getData();
                adapter.setList(peopleModels);
                adapter.notifyDataSetChanged();

            }



            @Override
            public void onFailure(Call<PeopleModel> call, Throwable t) {
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        return view;
    }




}
