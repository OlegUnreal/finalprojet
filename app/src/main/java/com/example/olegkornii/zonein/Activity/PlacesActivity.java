package com.example.olegkornii.zonein.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.olegkornii.zonein.Adapters.AdapterPeople;
import com.example.olegkornii.zonein.Adapters.AdapterPlaces;
import com.example.olegkornii.zonein.Models.Constants;
import com.example.olegkornii.zonein.Models.LocationService;
import com.example.olegkornii.zonein.Models.PeopleModel;
import com.example.olegkornii.zonein.Models.PlaceRequest;
import com.example.olegkornii.zonein.Models.PlacesModel;
import com.example.olegkornii.zonein.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlacesActivity extends AppCompatActivity {
    private String objectId;
    private List<PlacesModel.ItemPlace> placeModel = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        setTitle("Places");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewPlaces);
        final AdapterPlaces adapter = new AdapterPlaces(placeModel);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LocationService service = retrofit.create(LocationService.class);
        final Call<PlacesModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
        user.enqueue(new Callback<PlacesModel>() {
            @Override
            public void onResponse(Call<PlacesModel> call, Response<PlacesModel> response) {
                PlacesModel people = response.body();
                placeModel = people.getData();
                adapter.setList(placeModel);
                adapter.notifyDataSetChanged();
                adapter.setOnClickListener(new AdapterPlaces.OnClickListener() {
                    @Override
                    public void onClick(final int position) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(PlacesActivity.this, R.style.AppTheme_AppBarOverlay)
                                .setTitle("Show photo on map or fullscreen?")
                                .setPositiveButton("Show photo on map", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(PlacesActivity.this, MapsActivity.class);
                                        intent.putExtra("CityName", placeModel.get(position).getName());
                                        intent.putExtra("Lat", placeModel.get(position).getLatitude().toString());
                                        intent.putExtra("Lon", placeModel.get(position).getLongitude().toString());
                                        startActivity(intent);

                                    }
                                })
                                .setNegativeButton("Show photo fullscreen", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(PlacesActivity.this, PhotoActivityShow.class);
                                        intent.putExtra("idPl", placeModel.get(position).getObjectId());
                                        startActivity(intent);
                                    }
                                });
                        builder.show();
                    }

                    @Override
                    public void onLongClick(int position) {
                        objectId = placeModel.get(position).getObjectId();
                        Intent intent = new Intent(PlacesActivity.this, ImageLoadingActivity.class);
                        intent.putExtra("OID", objectId);
                        startActivity(intent);
                    }
                });

            }

            @Override
            public void onFailure(Call<PlacesModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
