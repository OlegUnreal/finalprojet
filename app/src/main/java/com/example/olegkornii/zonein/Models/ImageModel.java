package com.example.olegkornii.zonein.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Oleg Kornii on 18.08.2016.
 */
public class ImageModel implements Parcelable {
    private List<ItemImage> data;

    protected ImageModel(Parcel in) {
        data = in.createTypedArrayList(ItemImage.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<ImageModel> CREATOR = new Parcelable.Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel in) {
            return new ImageModel(in);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };

    public List<ItemImage> getData() {
        return data;
    }

    public static class ItemImage implements Parcelable {
        private String fullPath,
                connectedPlace,
                ownerId;

        public String getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(String ownerId) {
            this.ownerId = ownerId;
        }

        public String getFullPath() {
            return fullPath;
        }

        public void setFullPath(String fullPath) {
            this.fullPath = fullPath;
        }

        public String getConnectedPlace() {
            return connectedPlace;
        }

        public void setConnectedPlace(String connectedPlace) {
            this.connectedPlace = connectedPlace;
        }

        protected ItemImage(Parcel in) {
            fullPath = in.readString();
            connectedPlace = in.readString();
            ownerId = in.readString();
        }

        public static final Creator<ItemImage> CREATOR = new Creator<ItemImage>() {
            @Override
            public ItemImage createFromParcel(Parcel in) {
                return new ItemImage(in);
            }

            @Override
            public ItemImage[] newArray(int size) {
                return new ItemImage[size];
            }
        };


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(fullPath);
            dest.writeString(connectedPlace);
            dest.writeString(ownerId);

        }
    }
}
