package com.example.olegkornii.zonein.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.olegkornii.zonein.Adapters.AdapterPlaces;
import com.example.olegkornii.zonein.Models.CityLocation;
import com.example.olegkornii.zonein.Models.Constants;
import com.example.olegkornii.zonein.Models.LocationService;
//import com.example.olegkornii.zonein.Models.PlaceRequest;
import com.example.olegkornii.zonein.Models.PlacesModel;
import com.example.olegkornii.zonein.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private List<PlacesModel.ItemPlace> placeModel = new ArrayList<>();

    private String
            getLon,
            getLat;
    private Double
            getDoubleLon,
            getDoubleLat;
    private String getCityName;
    private GoogleMap mMap;
    private Button
            butSearch;
    private EditText
            cityname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        butSearch = (Button) findViewById(R.id.butSearch);
        cityname = (EditText) findViewById(R.id.etCity);
        getLat = getIntent().getStringExtra("Lat");
        getLon = getIntent().getStringExtra("Lon");
        getCityName = getIntent().getStringExtra("CityName");
        if (getLat != null && getLon != null && getCityName != null) {
            getDoubleLat = Double.valueOf(getLat);
            getDoubleLon = Double.valueOf(getLon);
        }

    }


            /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                if (getLat == null && getLon == null && getCityName == null) {

                    final Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://api.backendless.com/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    LocationService service = retrofit.create(LocationService.class);
                    final Call<PlacesModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                    user.enqueue(new Callback<PlacesModel>() {
                        @Override
                        public void onResponse(Call<PlacesModel> call, Response<PlacesModel> response) {
                            PlacesModel people = response.body();
                            placeModel = people.getData();
                            for (int i = 0; i < placeModel.size(); i++) {
                                LatLng city = new LatLng(Double.valueOf(placeModel.get(i).getLatitude()), Double.valueOf(placeModel.get(i).getLongitude()));
                                mMap.addMarker(new MarkerOptions()
                                        .position(city)
                                        .title(placeModel.get(i).getName())
                                );
                            }
                        }

                        @Override
                        public void onFailure(Call<PlacesModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });


                    butSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl("http://api.openweathermap.org/")
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            LocationService service = retrofit.create(LocationService.class);
                            Call<CityLocation> weather = service.getWeatherByName(cityname.getText().toString(), Constants.API_APPLICATION_ID);
                            weather.enqueue(new Callback<CityLocation>() {
                                @Override
                                public void onResponse(Call<CityLocation> call, Response<CityLocation> response) {
                                    CityLocation cityCoords = response.body();
                                    LatLng latLng = new LatLng(cityCoords.getCoord().getLat(), cityCoords.getCoord().getLon());
                                    mMap.addMarker(new MarkerOptions().position(latLng).title(cityname.getText().toString()));
                                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                }

                                @Override
                                public void onFailure(Call<CityLocation> call, Throwable t) {
                                    Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT);
                                }
                            });
                        }
                    });
                    mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                        @Override
                        public void onMapLongClick(LatLng latLng) {
                            LatLng city = new LatLng(latLng.latitude, latLng.longitude);
                            mMap.addMarker(new MarkerOptions().position(city).title("My place"));
                            Intent intent = new Intent(MapsActivity.this, NewPlaceActivity.class);
                            intent.putExtra("lat", latLng.latitude);
                            intent.putExtra("long", latLng.longitude);
                            startActivity(intent);
                        }
                    });
                } else {


                    LatLng city = new LatLng(getDoubleLat, getDoubleLon);

                    mMap.addMarker(new MarkerOptions()
                            .position(city)
                            .title(getCityName)
                    );
                    mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                        @Override
                        public void onMapLongClick(LatLng latLng) {
                            LatLng city = new LatLng(latLng.latitude, latLng.longitude);
                            mMap.addMarker(new MarkerOptions().position(city).title("My place"));
                            Intent intent = new Intent(MapsActivity.this, NewPlaceActivity.class);
                            intent.putExtra("lat", latLng.latitude);
                            intent.putExtra("long", latLng.longitude);
                            startActivity(intent);
                        }
                    });
                }

            }
}