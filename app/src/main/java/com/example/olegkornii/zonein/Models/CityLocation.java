package com.example.olegkornii.zonein.Models;

import android.provider.ContactsContract;

import java.util.Date;

/**
 * Created by Oleg Kornii on 03.08.2016.
 */
public class CityLocation {
    private Main main;

    public Main getMain() {
        return main;
    }

    public class Main{
        private  float temp;
        private  float humidity;

        public float getTemp() {
            return temp;
        }

        public float getHumidity() {
            return humidity;
        }

    }
    private  Coord coord;
    public  Coord getCoord() {
        return coord;
    }
    public class Coord {
        private float lon,lat;

        public float getLon() {
            return lon;
        }

        public float getLat() {
            return lat;
        }
    }
    private Data data;

    public Data getData() {
        return data;
    }
    public class Data {
        private String
        objectID,
        name;

        public String getObjectID() {
            return objectID;
        }

        public String getName() {
            return name;
        }
    }
}

