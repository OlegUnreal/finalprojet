package com.example.olegkornii.zonein.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.olegkornii.zonein.Models.PeopleModel;
import com.example.olegkornii.zonein.R;

import java.util.List;

/**
 * Created by Oleg Kornii on 05.08.2016.
 */
public class AdapterPeople extends RecyclerView.Adapter<AdapterPeople.ItemViewHolder> {
    private List<PeopleModel.ItemPeople> itemModelList;
    private OnClickListener listener;

    public AdapterPeople(List<PeopleModel.ItemPeople> itemModelList) {
        this.itemModelList = itemModelList;
    }

    public void setList(List<PeopleModel.ItemPeople> itemModelList) {
        this.itemModelList = itemModelList;
    }

    @Override
    public AdapterPeople.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemViewHolder holder = new ItemViewHolder(layoutInflater.inflate(R.layout.item_people, null));
        return holder;
    }

    @Override
    public void onBindViewHolder(AdapterPeople.ItemViewHolder holder, final int position) {
        final PeopleModel.ItemPeople itemModel = itemModelList.get(position);
        holder.name.setText(itemModel.getName());
        holder.email.setText(itemModel.getEmail());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (listener != null) {
                    listener.onLongClick(position);
                }
                return true;
            }
        });

    }


    @Override
    public int getItemCount() {
        return itemModelList != null ? itemModelList.size() : 0;
    }


    public interface OnClickListener {
        public void onClick(int position);


        public void onLongClick(int position);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.listener = onClickListener;


    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView
                name,
                email;

        public ItemViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById((R.id.tvName));
            email = (TextView) itemView.findViewById(R.id.tvEmail);
        }
    }
}