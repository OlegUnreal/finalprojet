package com.example.olegkornii.zonein.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.olegkornii.zonein.Models.ImageModelForShow;
import com.example.olegkornii.zonein.R;

import java.util.List;

/**
 * Created by Oleg Kornii on 20.08.2016.
 */
public class AdapterShowImage extends RecyclerView.Adapter<AdapterShowImage.ItemViewHolder> {
    private List<ImageModelForShow> itemModelList;
    private OnClickListener listener;
    private Context c;

    public RecyclerView recyclerView;
    public AdapterShowImage(List<ImageModelForShow> itemModelList, Context applicationContext) {
        this.itemModelList = itemModelList;
    }

    @Override
    public AdapterShowImage.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        c = layoutInflater.getContext();
        ItemViewHolder holder = new ItemViewHolder(layoutInflater.inflate(R.layout.activity_show_images, null));
        return holder;
    }

    @Override
    public void onBindViewHolder(AdapterShowImage.ItemViewHolder holder, final int position) {
        final ImageModelForShow itemModel = itemModelList.get(position);
        holder.linkPhoto.setText(itemModelList.get(position).getLinkPhoto());
        Glide
                .with(c)
                .load(itemModel.getLinkPhoto())
                .into(holder.showPhotos);

        holder.linkPhoto.setText(itemModelList.get(position).getLinkPhoto());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (listener != null) {
                    listener.onLongClick(position);
                }
                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return itemModelList != null ? itemModelList.size() : 0;
    }


    public interface OnClickListener {
        public void onClick(int position);

        public void onLongClick(int position);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.listener = onClickListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView showPhotos;
        private TextView linkPhoto;

        public ItemViewHolder(View itemView) {
            super(itemView);
            showPhotos = (ImageView) itemView.findViewById((R.id.showPhotos));
            linkPhoto = (TextView) itemView.findViewById((R.id.linkPhoto));
        }
    }
}
