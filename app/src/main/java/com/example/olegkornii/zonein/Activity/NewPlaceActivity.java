package com.example.olegkornii.zonein.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.provider.SyncStateContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.bumptech.glide.Glide;
import com.example.olegkornii.zonein.Models.CityLocation;
import com.example.olegkornii.zonein.Models.Constants;
import com.example.olegkornii.zonein.Models.LocationService;
import com.example.olegkornii.zonein.Models.PlaceRequest;
import com.example.olegkornii.zonein.Models.PlacesModel;
import com.example.olegkornii.zonein.R;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NewPlaceActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText
            etName,
            etDescription;
    private TextView
            tvLat,
            tvLong;
    private Button
            bCreate;
    private double
            latitude,
            longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_place);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        latitude = getIntent().getDoubleExtra("lat", 0.0);
        longitude = getIntent().getDoubleExtra("long", 0.0);
        initUI();
        setUI();
        setListeners();
    }


    private void createPlace() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.backendless.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LocationService service = retrofit.create(LocationService.class);
        PlaceRequest request = new PlaceRequest(
                etName.getText().toString(),
                etDescription.getText().toString(),
                latitude,
                longitude);
        Log.e("TAG", new Gson().toJson(request));
        Call<PlacesModel> userInfo = service.createPlace(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY, request);
        userInfo.enqueue(new Callback<PlacesModel>() {

            @Override
            public void onResponse(Call<PlacesModel> call, Response<PlacesModel> response) {
                Log.e("TAG", String.valueOf(response.raw()));
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "City " + etName.getText() + " added successfully!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlacesModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setUI() {
        tvLat.setText(String.valueOf("Latitude: " + latitude));
        tvLong.setText(String.valueOf("Longitude: " + longitude));
    }

    private void initUI() {
        etName = (EditText) findViewById(R.id.etCityName);
        etDescription = (EditText) findViewById(R.id.etCityDiscription);
        tvLat = (TextView) findViewById(R.id.tvLat);
        tvLong = (TextView) findViewById(R.id.tvLong);
        bCreate = (Button) findViewById(R.id.bCreate);
    }


    private void setListeners() {
        bCreate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bCreate:
                createPlace();
                break;
        }
    }
}
