package com.example.olegkornii.zonein.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.olegkornii.zonein.Adapters.AdapterFriends;
import com.example.olegkornii.zonein.Models.Constants;
import com.example.olegkornii.zonein.Models.FriendsModel;
import com.example.olegkornii.zonein.Models.LocationService;
import com.example.olegkornii.zonein.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class FriendsFragment extends Fragment {
    private String objectIdForFriend;

    private List<FriendsModel.ItemPeople> peopleModels = new ArrayList<>();

    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewFriend);
        final AdapterFriends adapter = new AdapterFriends(peopleModels);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.backendless.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LocationService service = retrofit.create(LocationService.class);
        final Call<FriendsModel> user = service.getFriendsByAppId(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
        user.enqueue(new Callback<FriendsModel>() {
            @Override
            public void onResponse(Call<FriendsModel> call, Response<FriendsModel> response) {
                FriendsModel people = response.body();
                peopleModels = people.getData();
                adapter.setList(peopleModels);
                adapter.notifyDataSetChanged();
            }


            @Override
            public void onFailure(Call<FriendsModel> call, Throwable t) {
                Toast.makeText(getActivity(), "Fault", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }


}
